export interface IUserData {
    id: number;
    name: string;
    lastName: string;
    email: string;
    age: number;
    }