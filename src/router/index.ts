import { createRouter, createWebHistory, type RouteRecordRaw } from "vue-router";
import Home from '@/views/HomeView.vue';
import ModifyUserData from '@/views/ModifyUserData.vue';



const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/edit/:userId',
    name: 'ModifyUser',
    component: ModifyUserData,
    props: true
  }
];

const router = createRouter({
  history: createWebHistory('/'),
  routes
});

export default router;

